-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2016 at 01:47 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `quiz_a`
--

CREATE TABLE `quiz_a` (
  `idx` int(11) NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quiz_a`
--

INSERT INTO `quiz_a` (`idx`, `question`, `answer`, `value`) VALUES
(1, 'What is HTML standing for?', 'It stands for Hyper Text Markup Language', 1),
(2, 'What is CSS standing for?', 'It stands for cascading Style Sheets', 1),
(3, 'What is JSON standing for?', 'It is JavaScript Object Notation', 1),
(4, 'What is JSON purpose?', 'It is data-interchange format', 1),
(5, 'What is Continous Integration?', 'It is the merging of all developer working copies to a shared mainline several times a day', 3),
(6, 'What is Continuous Deployment?', 'It is automatical deployment of the product into production whenever it passes QA', 1),
(7, 'What is Agile Software Development?', 'It describes a set of principles for software development under which requirements and solutions evolve through the collaborative effort of self-organizing cross-functional teams', 1),
(8, 'What is JavaScript?', 'It is a programming language', 2),
(9, 'What is love?', 'Baby don''t hurt me', 4),
(10, 'What is API standing for?', 'It stands for application programming interface', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_b`
--

CREATE TABLE `quiz_b` (
  `idx` int(11) NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quiz_b`
--

INSERT INTO `quiz_b` (`idx`, `question`, `answer`, `value`) VALUES
(1, 'What is CRUD?', 'It stands for create, remove, update and delete', 1),
(2, 'What is a RESTful web service?', 'It is a way of providing interoperability between computer systems on the Internet', 1),
(3, 'What is AngularJS', 'It is JavaScript-based open-source framework created by Google', 1),
(4, 'What is ReactJS	?', 'It is JavaScript-based open-source framework created by Facebook', 2),
(5, 'What is EmberJS?', 'It is JavaScript-based open-source framework created by Ember Core Team', 1),
(6, 'What is BackboneJS?', 'It is JavaScript-based open-source framework created by Jeremy Ashkenas', 3),
(7, 'What is Git?', 'It is a version control system', 1),
(8, 'What is Eclipse?', 'It is a integrated development enviroment', 1),
(9, 'What is IDE standing for?', 'It is standing for integrated development enviroment', 1),
(10, 'What is GitHub?', 'It is a web-based Git repository hosting service', 1);

-- --------------------------------------------------------

--
-- Table structure for table `saved_results`
--

CREATE TABLE `saved_results` (
  `idx` int(11) NOT NULL,
  `userName` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `quizName` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pointsCollected` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pointsMax` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `correctAnswerCount` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `quizLength` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `saved_results`
--

INSERT INTO `saved_results` (`idx`, `userName`, `quizName`, `pointsCollected`, `pointsMax`, `correctAnswerCount`, `quizLength`) VALUES
(90, 'Princess Mononoke', 'Quiz A', '4', '12', '1', '10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `quiz_a`
--
ALTER TABLE `quiz_a`
  ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `quiz_b`
--
ALTER TABLE `quiz_b`
  ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `saved_results`
--
ALTER TABLE `saved_results`
  ADD PRIMARY KEY (`idx`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quiz_a`
--
ALTER TABLE `quiz_a`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `quiz_b`
--
ALTER TABLE `quiz_b`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `saved_results`
--
ALTER TABLE `saved_results`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
