app.controller('buttonController', function ($rootScope) {
		var rs = $rootScope;
		rs.btnAction = function (button) {
		switch(rs.quizStage) {
		case 'inProgress':
			if (button == 'left') {
				rs.questionIndex--;
				localStorage.setItem('questionIndex', rs.questionIndex);
			} else {
				rs.questionIndex++;
				localStorage.setItem('questionIndex', rs.questionIndex);
			}			
			break;
		case 'showResult':
			if (button == 'left') {
				rs.startOver();
			} else {
				rs.isSaved ?
					rs.quizStage = "showSavedResults":
					rs.quizStage = 'saveResult';
				localStorage.setItem('quizStage', rs.quizStage)
			}
			break;
		case 'saveResult':
			if (button == 'left') {
				rs.startOver();
			} else {
				rs.quizStage = 'showResult';
				localStorage.setItem('quizStage', rs.quizStage)	
			}
			break;
		case 'showSavedResults':
			if (button == 'left') {
				rs.startOver();
			} else {
				rs.quizStage = 'showResult';
				localStorage.setItem('quizStage', rs.quizStage)
			}
			break;
		}
	};
})