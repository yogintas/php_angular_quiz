app.controller('quizController', function($rootScope) {
	var rs = $rootScope;	

	rs.userName = '';
	rs.quizName = function (quizName) {
		return localStorage.getItem("quizName") || localStorage.setItem('quizName', quizName);
	};
	rs.userData = {};
	rs.userData.userData = [];
	
	if (localStorage.length == 0) {
		rs.quizStage = '';
		rs.questionIndex = 0;
		rs.isSaved = 0;
		rs.quizData = [];
		rs.selectedAnswers = [];
	} else {
		rs.quizStage = localStorage.getItem('quizStage');
		rs.questionIndex = parseInt(localStorage.getItem('questionIndex')) || 0;
		rs.isSaved = localStorage.getItem('isSaved');
		rs.quizData = JSON.parse(localStorage.getItem('quizData'));
		rs.selectedAnswers = JSON.parse(localStorage.getItem('selectedAnswers')) || [];
		rs.evaluatedResult = JSON.parse(localStorage.getItem('evaluatedResult'));
		rs.savedResults = JSON.parse(localStorage.getItem('savedResults'));
		rs.quizDatabaseName = localStorage.getItem('quizDatabaseName');
	}

	rs.updateQuiz = function () {
		var quizData =  rs.quizData[rs.questionIndex];
		switch(rs.quizStage ) {
		case '':
			rs.header = 'Select Quiz to begin:';
			break;
		case 'inProgress':
			rs.questionIndex > rs.quizData.length-1 ?
				rs.quizStage ='showResult':'';
			rs.questionIndex <= rs.quizData.length-1 ?
				(rs.header = 'Q'+(rs.questionIndex+1)+': '+quizData.question+' ('+quizData.value+' pts.)') &&
				(rs.leftBtn = 'Previous') &&
				(rs.rightBtn = 'Next'):'';
			rs.questionIndex === rs.quizData.length-1 ?
				rs.rightBtn = 'Submit':'';
			break;
		case 'showResult':
			rs.header = 'Results ('+rs.quizName()+')';
			rs.leftBtn='Start Over';
			rs.isSaved ?
				rs.rightBtn = 'Saved Results':
				rs.rightBtn = 'Save Result';
			rs.generateResult()
			break;
		case 'saveResult':
			rs.header = 'Save Result';
			rs.leftBtn='Start Over';
			rs.rightBtn = 'Show Result';
			break;
		case 'showSavedResults':
		 	rs.header = 'Saved Results';
		 	rs.leftBtn='Start Over';
			rs.rightBtn = 'Show Result';
			rs.getSavedResults();
			break;
		}
	}
	rs.selectAnswer = function (index) {
		rs.selectedAnswers[rs.questionIndex] = index;
		localStorage.setItem('selectedAnswers', JSON.stringify(rs.selectedAnswers) );
	}
	rs.startOver = function () {
		localStorage.clear();
		rs.quizData = [];
		rs.selectedAnswers = [];
		rs.userData = {};
		rs.userData.userData = [];
		rs.evaluatedResult = [];
		rs.savedResults = [];
		rs.quizStage = '';
		rs.questionIndex = 0;
		rs.isSaved = 0;
	}
	rs.$watchGroup(['questionIndex', 'quizStage'], function () {
		rs.updateQuiz();
	})
});