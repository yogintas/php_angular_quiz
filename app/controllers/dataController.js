app.controller('dataController', function ($rootScope, ajaxService) {
	var rs = $rootScope;

	rs.getQuizData = function (quizDatabaseName) {
		rs.quizDatabaseName = {
			quizDatabaseName: quizDatabaseName
		};
		localStorage.setItem('quizDatabaseName', quizDatabaseName);
		ajaxService.getQuizData(rs.quizDatabaseName)
		.then(function (data) {
			rs.quizData = data;
			rs.quizStage = 'inProgress';
			localStorage.setItem('quizData', JSON.stringify(data) );
			localStorage.setItem('quizStage', rs.quizStage);
		})		
	};

	rs.generateResult = function () {
		if (localStorage.getItem('evaluatedResult') != null) {
			rs.evaluatedResult = JSON.parse(localStorage.getItem('evaluatedResult'))
		} else {
			for (var i = 0; i < rs.quizData.length; i++) {
			 	rs.userData.userData[i] = {
			 		idx: rs.quizData[i].idx,
			 		userAnswer: rs.quizData[i].answer[rs.selectedAnswers[i]] || 'No Answer.'
			 	}
			 }
			rs.userData.quizDatabaseName = rs.quizDatabaseName;
			ajaxService.generateResult(rs.userData)
			.then(function (data) {
				rs.evaluatedResult = data;
				localStorage.setItem( 'evaluatedResult', JSON.stringify(data) );
			})
		}
	};

	rs.saveResult = function () {
		var dataToSave = rs.evaluatedResult.evaluatedUserData;
		dataToSave[0].userName = rs.userName || 'Someone';
		dataToSave[0].quizName = rs.quizName();
		ajaxService.saveResult(dataToSave)
			.then(function(data){
				rs.isSaved = 1;
				rs.quizStage = 'showSavedResults';
				localStorage.setItem('isSaved', rs.isSaved);
				localStorage.setItem('quizStage', rs.quizStage);
			})
	}

	rs.getSavedResults = function () {
		if (localStorage.getItem('savedResults') != null) { 
			rs.savedResults = JSON.parse(localStorage.getItem('savedResults'));
		} else {
			ajaxService.getSavedResults()
			.then(function(data){
				rs.savedResults = data;
				localStorage.setItem('savedResults', JSON.stringify(data) );
			});
		}
	}
})
