app.factory("ajaxService", function($http, $q) {
	return {
		getQuizData: function (quizDatabaseName) {
			return $http.post("php/services/getQuizDataService.php", quizDatabaseName)
			.then(
				function successCallback(response) {
					return response.data;
				},
				function errorCallback (response) {
				}
			)			
		},
		generateResult: function (userData) {
			return $http.post("php/services/generateResultService.php", userData)
			.then(
				function successCallback(response) {
					return response.data;
				},
				function errorCallback (response) {
				}
			)
		},
		saveResult: function (dataToSave) {
			return $http.post("php/services/saveResultService.php", dataToSave)
			.then(
				function successCallback(response) {
					return response.data;
				},
				function errorCallback (response) {
				}
			)
		},
		getSavedResults: function () {
			return $http.post("php/services/getSavedResultsService.php")
			.then(
				function successCallback(response) {
					return response.data;
				},
				function errorCallback (response) {
				}
			)
		}
	}	
});	