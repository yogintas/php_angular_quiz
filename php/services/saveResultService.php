<?php

require_once '../classes/_MySQLi_class.php';

$evaluatedUserData = json_decode(file_get_contents('php://input'), true);

$query="
INSERT INTO saved_results (
	userName, quizName, pointsCollected, pointsMax, correctAnswerCount, quizLength
)
VALUES 	(
	'".$evaluatedUserData[0]['userName']."',
	'".$evaluatedUserData[0]['quizName']."',
	".$evaluatedUserData[0]['pointsCollected'].",
	".$evaluatedUserData[0]['pointsMax'].",
	".$evaluatedUserData[0]['correctAnswerCount'].",
	".$evaluatedUserData[0]['quizLength']."
)";

_MySQLi::execQuery($query);

echo "Success.";
?>