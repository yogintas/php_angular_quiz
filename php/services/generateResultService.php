<?php

require_once '../classes/_MySQLi_class.php';

$pointsCollected = 0;
$pointsMax = 0;
$correctAnswerCount = 0;

$userData = json_decode(file_get_contents('php://input'), true);

$query="SELECT question, answer, value FROM ".$userData['quizDatabaseName']['quizDatabaseName']."";

$table_array = _MySQLi::getTable($query);

for ($i=0; $i < sizeof($userData['userData']); $i++) {
	$answerIndex = $userData['userData'][$i]['idx']-1;
	if($userData['userData'][$i]['userAnswer'] !== $table_array[$answerIndex]['answer']) {
		$evaluatedUserAnswers[$i]['question'] = $table_array[$answerIndex]['question'];
		$evaluatedUserAnswers[$i]['userAnswer'] = $userData['userData'][$i]['userAnswer'];
		$evaluatedUserAnswers[$i]['correctAnswer'] = $table_array[$answerIndex]['answer'];
		$evaluatedUserAnswers[$i]['value'] = $table_array[$answerIndex]['value'];
		$pointsMax += $table_array[$answerIndex]['value'];
	} else {
		$evaluatedUserAnswers[$i]['question'] = $table_array[$answerIndex]['question'];
		$evaluatedUserAnswers[$i]['correctAnswer'] = $table_array[$answerIndex]['answer'];
		$evaluatedUserAnswers[$i]['value'] = $table_array[$answerIndex]['value'];
		$pointsCollected += $table_array[$answerIndex]['value'];
		$correctAnswerCount++;
	}
}

$evaluatedUserData = [[	
	'correctAnswerCount' => $correctAnswerCount,
	'quizLength' => sizeof($userData['userData']),
	'pointsCollected' => $pointsCollected,
	'pointsMax' => $pointsMax
]];

$result['evaluatedUserAnswers'] = $evaluatedUserAnswers;
$result['evaluatedUserData'] = $evaluatedUserData;

header('Content-Type: application/json');

echo $json_response = json_encode($result);

?>
