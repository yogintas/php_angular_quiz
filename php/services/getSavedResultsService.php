<?php

require_once '../classes/_MySQLi_class.php';
 
$query="SELECT * FROM saved_results";

$savedResults = _MySQLi::getTable($query);

header('Content-Type: application/json');
echo $json_response = json_encode($savedResults);

?>