<?php
require_once '../classes/_MySQLi_class.php';

$quizDatabaseName = json_decode(file_get_contents('php://input'), true);

$query="SELECT * FROM ".$quizDatabaseName['quizDatabaseName']."";

$tempArr = _MySQLi::getTable($query);
$quizArr = $tempArr;

for ($i=0; $i < sizeof($tempArr); $i++) {
	$choices = array();
	array_push($choices, $tempArr[$i]['answer']);
	while (sizeof($choices) < 4) {
		$rnd_num = floor(mt_rand(0, (sizeof($tempArr)-1)));

		if ($rnd_num == $i) continue;

		if ( in_array($tempArr[$rnd_num]['answer'], $choices) ) continue;

		$choices[sizeof($choices)] = $tempArr[$rnd_num]['answer'];
	}

	shuffle($choices);

	$quizArr[$i]['answer'] = $choices;
}

shuffle($quizArr);

header('Content-Type: application/json');
echo $json_response = json_encode($quizArr);
?>
