<?php

class _MySQLi {

	private static $HOST = 'localhost';
	private static $USER = 'root';
	private static $PASSWORD = '';
	private static $DB_NAME = 'quiz_data';
	private static $CHARSET = "utf8";

	static public $connect=null;
	
	static public function connect(){
		self::$connect=mysqli_connect(self::$HOST, self::$USER, self::$PASSWORD);
		mysqli_select_db(self::$connect, self::$DB_NAME);
		mysqli_set_charset(self::$connect, self::$CHARSET);
	}

	static public function getTable($query){
		if(self::$connect==null) self::connect();
		$result=mysqli_query(self::$connect, $query);
		$rows=array();
		while($row=mysqli_fetch_assoc($result)){
			$rows[]=$row;
		}
		return $rows;
	}

	static public function getRow($query) {
		if(self::$connect==null) self::connect();
		$result=mysqli_query(self::$connect, $query);
		return mysqli_fetch_assoc($result);
	}
	
	static public function execQuery($query){
		if(self::$connect==null) self::connect();
		$result=mysqli_query(self::$connect, $query);
	}	
}

?>